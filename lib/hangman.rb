class Hangman
  attr_reader :guesser, :referee
  attr_accessor :board

  def initialize(players={guesser: nil, referee: nil})
    @board = self.board
    self.board = []
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
      puts "welcome to Hangman, Ref will now pick secret word"
      length = self.referee.pick_secret_word
      self.guesser.register_secret_length(length)
      puts "The current secret word has a length of #{length}"
      length.times {self.board << " _ "}
      puts self.board.join

  end

  def take_turn
    puts "Now, guess a letter from the secret word."
    currentguess = self.guesser.guess(board)
    puts "Interesting choice...Let's see if our word contains '#{currentguess}'..."
    indices = self.referee.check_guess(currentguess)
    if indices == []
      puts "Try again! Your letter is not in the word."
    else
      indices.each {|num| puts "your letter appears at #{num}"}
    end
    self.class.update_board(@board, currentguess, indices)
    self.guesser.handle_response(currentguess, indices)
  end

  def self.update_board(*anyargs)
    board = anyargs[0]
    letter = anyargs[1]
    indices = anyargs[2]
    if indices != []
      board.map!.each_with_index do |el,idx|
          if indices.include?(idx)
            letter
          else
            el
          end
      end
    end
    puts board.join
  end

end

class HumanPlayer

  def initialize(dictionary)
  @currentword = ""
  end

  def register_secret_length(num)

  end

  def pick_secret_word
    @currentword = gets.chomp
    @currentword.length
  end

  def candidate_words

  end

  def guess(board)
    gets.chomp
  end

  def check_guess(string)
    foundletter = []
    return foundletter if !@currentword.chars.include?(string)
    @currentword.chars.each_with_index { |letter, idx|
      foundletter << idx if letter == string }
      foundletter
  end

  def handle_response(string, array)

  end

end

class ComputerPlayer
  attr_accessor :dictionary

  def initialize(dictionary)
    @dictionary = dictionary
    @currentword = ""
  end

  def register_secret_length(num)
    @dictionary.select! {|el| el.length == num}
  end

  def pick_secret_word
    @currentword = @dictionary[rand(0..dictionary.length-1)]
    # currentword = IO.readlines("/lib/dictionary.txt")[1]
    @currentword.length
  end

  def candidate_words
      @dictionary
  end

  def guess(board)
    #generate freq hash
    freqs = Hash.new(0)
      @dictionary.each do |el|
        el.chars.each {|char| freqs[char] += 1}
      end
    #if board is empty
    if board.all? { |el| el == " _ "}
      freqs.key(freqs.values.max)
    else
      #board is not empty
      #find out what letters are on board
      lettersonboard = board.reject {|el| el == " _ "}
      #reject words that have that letter
      lettersonboard.each do |letter|
        freqs.reject! { |k,v| k == letter}
      end
      freqs.key(freqs.values.max )
    end
  end

  def check_guess(string)
    foundletter = []
    return foundletter if !@currentword.chars.include?(string)
    @currentword.chars.each_with_index { |letter, idx|
      foundletter << idx if letter == string }
      foundletter
  end

  def handle_response(letter, indices)
    if indices == []
      #not found. get rid of words that have this letter
      candidate_words.reject! {|el| el.include?(letter)}
    else
      #select words that have this letter at targetindex
      indices.each do |targetindex|
        candidate_words.select! {|word| word[targetindex] == letter}
        #then, get rid of words that have more than the amount of matches
        candidate_words.reject! do |word|
          if word[targetindex] == letter
            word.count(letter) > indices.size
          end
        end
      end
    end
  end

end
